"""
Tutorial - Set of utilities to score a rock paper scissor game.

"""
ROCK = 'r'
PAPER = 'p'
SCISSOR = 's'

game_lookup = {
    (ROCK, ROCK): 0,
    (ROCK, PAPER): -1,
    (ROCK, SCISSOR): 1,

    (PAPER, ROCK): 1,
    (PAPER, PAPER): 0,
    (PAPER, SCISSOR): 0,

    (SCISSOR, ROCK): -1,
    (SCISSOR, PAPER): 1,
    (SCISSOR, SCISSOR): 0,
}

def score_turn(player_one, player_two):
    """
    Score a single turn of rock paper scissors using if statements.


    :param player_one:
    :param player_two:
    :return:    Possible outcomes:
    tie - return 0
    player_one won = return 1
    player_two won = return -1.
    """

    result = 0

    if player_one == ROCK:
        if player_two == PAPER:
            result = -1
        elif player_two == SCISSOR:
            result = 1
    elif player_one == PAPER:
        if player_two == ROCK:
            result = 1
        elif player_two == SCISSOR:
            result = -1
    elif player_one == SCISSOR:
        if player_two == ROCK:
            result = -1
        elif player_two == PAPER:
            result = 1

    return result

def score_turn_lookup(player_one, player_two):
    """
    Score a single turn of rock paper scissor.


    :param player_one:
    :param player_two:
    :return:    Possible outcomes:
    tie - return 0
    player_one won = return 1
    player_two won = return -1.
    """

    result = game_lookup[  (player_one, player_two)]


    return result


if __name__ == '__main__':
    player_one = ROCK
    player_two = PAPER

    result = score_turn(player_one, player_two)

    #Simple loop to test all possibilities.

    for player_one in [ROCK, PAPER, SCISSOR]:
        for player_two in [ROCK, PAPER, SCISSOR]:
            result = score_turn(player_one, player_two)
            result_two = score_turn_lookup(player_one, player_two)
            print("result player_one[{}] player_two[{}] = {} {}".format(player_one, player_two, result, result_two))


