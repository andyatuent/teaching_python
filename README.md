# teaching_python


Set of coded projects to help a beginner learn python. 




##Rock Paper Scissors (rps.py)

teaching:
1. start with if statement version.
   - Paper exercise - Work with student to develop pseudo code logic for each combination of pairs rock, paper, scissor
   -- key points: think through process first then code.    
   - Convert to py syntax - if, elif.  Introduce variables.  for ROCK, PAPER, SCISSOR
2.  Move to function 
   -- rps_round() after you have the basics of the code working.  
   -- in:  move by player_one vs player_two
   --- out: who won or tie.      simle response is   -1 = player_one lost, 1 = player_one won, 0 = tie. 

3. Make a simple game against computer (player_two)
   loop around input()s asking for player_one's score
   call rps_round() - keep score. 
   make sure there is enough logging so you can discuss logic each round. 
   player_two's input can be random.  Consider making another function.    
 
 4. add score mechanism to keep track. 
    - ask student - for random player-two input (what do they expect results to be.)  

Futures: 
5. Improve game - player_two (computer) can be more than random. 
  - Key point - introduce notion of an algorithm.       
  start here: 
  - https://www.technologyreview.com/s/527026/how-to-win-at-rock-paper-scissors/
  - https://towardsdatascience.com/how-to-win-over-70-matches-in-rock-paper-scissors-3e17e67e0dab
  
 
  

6. dictionary lookup for scoring function. 
  - Make code more concise ...  move logic to a dictionary - and allow lookup. 
  - One option is dictionary key is tuple of player_one  

